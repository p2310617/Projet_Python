import unittest 
from script import modelisation as md
import pandas as pd



class Test_modelisation(unittest.TestCase):

    def setUp(self) -> None:
        url1 = "https://api.lasocietenouvelle.org/macrodata/metadata/macro_fpt_a38"
        url2 = "https://api.lasocietenouvelle.org/macrodata/macro_fpt_a38"
        path_graph = "Projet_Python/Public/"
        path_data = "Projet_Python/Data/"
        data = pd.read_excel("Data/donnees.xlsx")

        return url1,url2, data
    def test_importdata(self):
        url1, url2, data = self.setUp()
        self.assertIsInstance(url1, str)
        self.assertIsInstance(url2, str)


        df1,df2 = md(url1, url2)
        data = md.traitement()
        self.assertIsInstance(df1, pd.DataFrame)
        self.assertIsInstance(df2, pd.DataFrame)
        self.assertIsInstance(data, pd.DataFrame)

