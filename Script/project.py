from script import modelisation


url1 = "https://api.lasocietenouvelle.org/macrodata/metadata/macro_fpt_a38"
url2 = "https://api.lasocietenouvelle.org/macrodata/macro_fpt_a38"
path_graph = "Projet_Python/Public/"
path_data = "Projet_Python/Data/"

md = modelisation(url1, url2)
df1, df2 = md.import_data()
md.enregistrement_data(path_data = 'Public/', df = df1, nom="donnees")
md.enregistrement_data(path_data = 'Public/', df = df2, nom="metadonnees")
data = md.traitement()
md.enregistrement_data(path_data = 'Public/', df = data, nom="BD_final")
md.compare_aggregates()
md.autoregressive_model()