# mypy: ignore_missing_imports
import requests
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import plotly.io as io
import plotly.graph_objects as go
import statsmodels.api as smt
import os
# url1 = "https://api.lasocietenouvelle.org/macrodata/metadata/macro_fpt_a38"
# url2 = "https://api.lasocietenouvelle.org/macrodata/macro_fpt_a38"
# path_graph = "Projet_Python/Public/"
# path_data = "Projet_Python/Data/"


class modelisation:
    def __init__(self, url1, url2):
        self.url1 = url1
        self.url2 = url2

    def import_data(self):
        """
        Fonction de scrapingdes données en utilisant API du site internet
        -------------------------------
        ARG:
        url1 (str): lien de API pour scraper les métadonnées
                     de notre base de données
        url2(str): lien de API pour scraper les données
        ------------------------------
        Return:
        df (pd.DataFrame): meta données des indicateurs et leur définition
        data_df (pd.DataFrame): la base de données
                                qu'on va utiliser dans notre projet
        """
        headers = {"accept": "application/json"}
        response = requests.get(self.url1, headers=headers)
        json_data = json.loads(response.content.decode('utf-8'))
        df = pd.concat([pd.json_normalize(json_data['metadata'][key])
                        .add_prefix(key + '_')
                        for key in json_data['metadata']], axis=1)
        # base des données des indicateurs et des branches

        response1 = requests.get(self.url2, headers=headers)
        data_dict = json.loads(response1.content)

        data_df = pd.DataFrame(data_dict['data'])
        data_df.drop(columns=['lastupdate', 'lastupload'], inplace=True)

        return df, data_df

    def enregistrement_data(self, path_data: str,
                            df: pd.DataFrame, nom: str) -> None:
        """
        Fonction d'enregistrement des données scrapées sous formats de Excel
        -------------------------------
        ARG:
        path_data (str): repertoire d'enregistrement des données
        df (pd.DataFrame): dataframe des métadonnées
                         et définitions des variables
        data_df (pd.DataFrame): les données utilisée dans notre analyse
        ------------------------------
        Return:
        None
        """
        os.chdir(path_data)
        df.to_excel(str(nom) + ".xlsx", index=False)

    def traitement(self):
        """
        Fonction de nettoyage des dataframe
        ---------------------
        ARGS:
        df, data_df (pd.DataFrame): la base de données et metadata
        ---------------------
        Return:
        data (pd.DataFrame): La base de données finale avec
                            la labélisation de chaque variable.
        """
        # Optionnel: soit importer les données en utilisant
        # la fonction deja code ou omporter les données enregistrer
        """
        df1 = pd.read_excel("Data\\donnees.xlsx")
        df2 = pd.read_excel("Data\\metadonnees.xlsx)
        """

        df1, df2 = self.import_data()
        columns_drop = "year_label"
        df1.drop(columns=columns_drop, inplace=True)
        # renommage des colonnes et debarasser des colonnes
        df1 = (df1.rename(columns={"branch_code": "branch",
                                   "indic_code": "indic",
                                   "unit_code": "unit",
                                   "aggregate_code": "aggregate",
                                   "area_code": "area",
                                   "flag_code": "flag",
                                   "currency_code": "currency",
                                   "year_code": "year"}))

        # préparation des données au merging
        branch = df1[["branch", "branch_label"]]
        unit = df1[["unit", "unit_label"]]
        aggregate = df1[["aggregate", "aggregate_label"]]
        indic = df1[["indic", "indic_label"]]
        flag = df1[["flag", "flag_label"]]
        currency = df1[["currency", "currency_label"]]
        area = df1[["area", "area_label"]]

        # merging de la base de données:
        data = (pd.merge(df2, branch, on="branch")
                .merge(unit, on="unit")
                .merge(aggregate, on="aggregate")
                .merge(indic, on="indic")
                .merge(flag, on="flag")
                .merge(currency, on="currency")
                .merge(area, on="area"))

        return data

    def compare_aggregates(self):
        """
        Fonction de statistique descriptive et de visualisation des données


        ARGS:
        ----------------------------------------
        None
        Returns:
        ---------------------------------------
        figures enregistrer dans le repertoire Public
        """
        data = self.traitement()
        data.dropna(inplace=True)
        data['year'] = data['year'].astype(int)
        data['value'] = data['value'].astype(int)
        df_1 = (data[data['year']
                .isin([2015, 2016, 2017, 2018, 2019, 2020, 2021])])
        df_2 = df_1.loc[:, ["year", "aggregate", "value"]]
        resultat = (df_2.groupby(['year', 'aggregate'])
                    .agg(moyenne_value=('value', 'mean')).reset_index())
        df_3 = (resultat.loc[resultat['year']
                .isin([2015, 2016, 2017, 2018, 2019, 2020, 2021])])
        df_3['moyenne_value'] = df_3['moyenne_value'].astype(int)
        Dis = (df_3[df_3['aggregate']
               .isin(["CFC", "NVA", "IC", "PRD", "TRESS", "GVA"])])

        df_pivot = df_3.pivot(index='year',
                              columns='aggregate',
                              values='moyenne_value')

        fig = px.line(Dis, x='year', y='moyenne_value', color='aggregate',
                      title="Évolution des différents agrégats"
                            "en fonction des années",
                      labels={'year': 'Année',
                              'moyenne_value': 'valeur moyenne des aggregats',
                              'aggregate': 'Agrégat'})

        fig.update_xaxes(type='category')
        fig.write_image("Public/comparaison_des_aggregats.pdf",
                        engine="kaleido")

        aggregate_mean = (self.data_df.groupby('aggregate')['value']
                          .mean().reset_index())
        aggregate_mean.columns = ['aggregate', 'moyenne_value']

        colors = {"CFC": "blue",
                  "NVA": "red",
                  "IC": "green",
                  "PRD": "purple",
                  "TRESS": "pink",
                  "GVA": "skyblue"}

        fig = go.Figure()

        for aggregate, color in colors.items():
            agg_data = aggregate_mean[aggregate_mean['aggregate'] == aggregate]
            fig.add_trace(go.Bar(x=agg_data['aggregate'],
                                 y=agg_data['moyenne_value'],
                                 name=aggregate, marker_color=color))

        fig.update_layout(xaxis_title="Agrégats",
                          yaxis_title="Valeur moyenne",
                          title="Valeur moyenne par agrégat",
                          barmode='group')
        fig.write_image("Public/diagrame des valeurs moyennes par agrégat.pdf",
                        engine="kaleido")

        correlation_matrix = df_pivot.corr()
        plt.figure(figsize=(10, 8))
        sns.heatmap(correlation_matrix, annot=True, cmap='coolwarm', fmt=".2f")
        plt.title('Matrice de corrélation')
        plt.show()
        plt.savefig('Public/matrice_de_correlation.pdf')

    def autoregressive_model(self):
        """
        Fonction pour modéliser un processus autoregressif
        ARGS:
        -------------------------
        None
        RETURNS:
        -------------------------
        Une image avec les résultats de modèle
        """
        data = self.traitement()
        data_cfc = data[data['indic'] == "GHG"]
        serie_temporelle = data_cfc['value']
        model = smt.AutoReg(serie_temporelle, lags=2)
        model_res = model.fit()

        summary = model_res.summary()
        print(summary)

        with open("Model_Summary.txt", "w") as file:
            file.write(summary.as_text())

        fig, ax = plt.subplots(figsize=(16, 8))
        ax.text(0.01, 0.05, summary.as_text(),
                fontfamily='monospace', fontsize=12)
        ax.axis('off')
        plt.tight_layout()
        plt.savefig("Resultats du modele.pdf", dpi=300, bbox_inches='tight')
